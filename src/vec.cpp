#include <iostream>
#include <cstdio>
#include <vector>
#include <cstring>

const size_t TEXT_SIZE = 256;

typedef struct
{
    int token_type;
    char text[TEXT_SIZE];
} Token;

Token read_token()
{
    Token token;
    token.token_type = 0;
    strcpy(token.text, "sample_variable");

    return token;
}

int main(void)
{
    std::vector<Token> tokens;

    for (int i = 0; i < 10; i++)
    {
        Token token = read_token();
        tokens.push_back(token);
    }

    for (int i = 0; i < 10; i++)
    {
        printf("Token{%d, %s}\n", tokens[i].token_type, tokens[i].text);
    }

    return 0;
}
