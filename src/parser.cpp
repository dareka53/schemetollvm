#include <iostream>
#include <cstdlib>
#include <cstring>
#include <ctype.h>
#include <vector>

#include "token.hpp"
#include "prot.hpp"

Token token2;
FILE* fp;

int main(int argc, char* argv[])
{
    if (argv[1] == NULL) { exit(1); }
    else {
        fp = fopen(argv[1], "r");
        std::vector<Token> tokens = lexer(fp);

        for(int i=0; tokens[i].tokenType != E_O_T; i++)
        {
            printf("%s:\n\t TokenType: %s\n", tokens[i].text, convertTokenType(tokens[i].tokenType));
        }
    }

    return 0;
}