#ifndef TOKEN_HPP
#define TOKEN_HPP

#define IDENT_SIZE 31
#define TEXT_SIZE 100

typedef enum {
    IDENTIFIER_TOKEN, // identifier
    BOOLEAN_TOKEN,    // boolean
    NUMBER_TOKEN,     // real number
    CHARACTER_TOKEN,  // character
    STRING_TOKEN,     // string
    LPAREN_TOKEN,     // "("
    RPAREN_TOKEN,     // ")"
    HASH_LPAREN_TOKEN,// "#("
    S_QUOTES_TOKEN,   // '
    B_QUOTES_TOKEN,   // "`"
    COMMA_TOKEN,      // ","
    COMMA_AT_TOKEN,   // ",@"
    DOT_TOKEN,        // "."

    LETTER,     // letter

    D_QUOTES,   // "\""
    COLON,      // ":"
    SEMICOLON,  // ";"

    HASH,       // "#"

    DELIMITER,

    LSQUARE,    // "["
    RSQUARE,    // "]"
    LBRACE,     // "{"
    RBRACE,     // "}"

    OTHERS,     // others
    NULLTYPE,   // null
    END_LIST,   // ""
    E_O_T,      // End of Token
} TokenType;


typedef struct {
    TokenType tokenType;
    char      text[IDENT_SIZE+1];
} Token;

#endif