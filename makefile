lexer: src/lexer.cpp
	g++ -o lexer.exe src/lexer.cpp && lexer.exe test.txt
parser: src/parser.cpp
	g++ -o parser.exe src/parser.cpp src/lexer.cpp && parser.exe test.txt
clean: 
	del lexer.exe parser.exe